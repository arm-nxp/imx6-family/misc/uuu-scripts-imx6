# conga-QMX6 related scripts for uuu tool from NXP

This repo contains helper scripts to boot and flash the bootloader to a conga-QMX6 module with NXP uuu utility.

See U-Boot README for details on howto build U-Boot for conga-QMX6 modules. To use this scripts,
all files have to be in the same folder. So either copy the uuu executable for your platform and
the .auto script to U-Boot build folder or copy the needed bootloader files, the .auto script and
the uuu tool in an extra folder and start uuu there.



## uuu
uuu [Universal Update Utility, mfgtools 3.0](https://github.com/nxp-imx/mfgtools) is the NXP tool for recovery boot on i.MX6 (and others). It is available on github and is used in version 1.4.243: [uuu version 1.4.243](https://github.com/NXPmicro/mfgtools/releases/tag/uuu_1.4.243)



## Scripts for SPL-Bootloader (default)

The SPL bootloader is the default bootloader and supports all conga-QMX6 standard products. This scripts uses the files which are generated on build process.

Flash or boot the bootloader to the module with the following scripts:

- **conga-qmx6-flash-spl.auto**
       Flash the SPL bootloader with the SPL bootloader. Three files are needed: `u-boot-with-spl.imx` to boot from, `SPL` and `u-boot.img` to flash to the module. All three files are generated automatically on uboot build.


 - **conga-qmx6-boot-spl.auto**
       Boot the SPL bootloader directly without flashing. It uses the file `u-boot-dtb.imx` which is generated on default SPL build.
       This is a replacement for `.\uuu.exe -b spl u-boot-dtb.imx`, but it stops at the end.



### Flash SPL bootloader
```
.\uuu.exe conga-qmx6-flash-spl.auto
```

### Boot SPL bootloader only
```
.\uuu.exe conga-qmx6-boot-spl.auto
```




## Scripts for non-SPL Bootloaders
Non-SPL bootloader is needed, if no MFG data is in the SPI MFG area or if the partnumber is unknown to the standard bootloader (eg. on a CSA module). The used bootloader file (`u-boot-with-spl.imx`) is generated automatically on non-SPL builds of conga-QMX6 bootloader.

ATTENTION:
       Be aware, that the non-SPL bootloader to use depends on your conga-QMX6 module.
       It differs from mounted RAM and CPU type (e.g SoloCore or QuadCore).
       Details can be found in the U-Boot README.


 - **conga-qmx6-boot-nonspl.auto**
       Boot the non-SPL bootloader directly without flashing.

 - **conga-qmx6-flash-nonspl.auto**
       Flash non-SPL bootloader with non-SPL bootloader.



### Flash non-SPL bootloader
```
.\uuu.exe conga-qmx6-boot-nonspl.auto
```

### Boot non-SPL bootloader only
```
.\uuu.exe conga-qmx6-flash-nonspl.auto
```


## Scripts for mixed setups

It is also possible to flash a SPL bootloader with a non-SPL one.

 - **conga-qmx6-flash-spl-from-nonspl.auto**. This needs three files:
       `u-boot-dtb.imx` is the non-SPL bootloader to boot,
       `SPL` and `u-boot.img` are the SPL bootloader to flash to spi-flash




